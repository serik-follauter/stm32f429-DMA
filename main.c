#include "stm32f429xx.h"
#include "main.h"

uint32_t T;
uint32_t Data[100];

ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma2;
TIM_HandleTypeDef htim2;

int main()
{
  HAL_Init();
  
  __HAL_RCC_GPIOA_CLK_ENABLE();  //clocking
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_ADC1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();
  __HAL_RCC_TIM2_CLK_ENABLE();
  
  GPIO_InitTypeDef gp;  //button config
  gp.Pin = GPIO_PIN_0;
  gp.Mode = GPIO_MODE_INPUT;
  HAL_GPIO_Init(GPIOA,&gp);
  
  gp.Pin = GPIO_PIN_13 | GPIO_PIN_14; //LEDs config
  gp.Mode = GPIO_MODE_OUTPUT_PP;
  HAL_GPIO_Init(GPIOG,&gp);
  
  //TIM2 to generate clocking for ADC with 1 Hz freq
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 4000 - 1;
  htim2.Init.Period = TIM2_PERIOD - 1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  HAL_TIM_Base_Init(&htim2);
  
  TIM_MasterConfigTypeDef mmstim2;      //HAL TIM EX, generate trigger signals upon update event
  mmstim2.MasterOutputTrigger = TIM_TRGO_UPDATE;
  mmstim2.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE; //??? master/slave mode selection ???
  HAL_TIMEx_MasterConfigSynchronization(&htim2, &mmstim2);
    
  hdma2.Instance = DMA2_Stream0;
  hdma2.Init.Channel = DMA_CHANNEL_0;
  hdma2.Init.Direction = DMA_PERIPH_TO_MEMORY;
  hdma2.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
  hdma2.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
  hdma2.Init.MemInc = DMA_MINC_ENABLE;
  hdma2.Init.Mode = DMA_CIRCULAR;
  hdma2.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
  HAL_DMA_Init(&hdma2);
  
  __HAL_LINKDMA(&hadc1, DMA_Handle, hdma2);
  
  //ADC1 to mesure temperature
  hadc1.Instance = ADC1;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T2_TRGO;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  HAL_ADC_Init(&hadc1);
  
  ADC_ChannelConfTypeDef chadc1; //18 channel
  chadc1.Channel = ADC_CHANNEL_18;
  chadc1.Rank = 1;
  HAL_ADC_ConfigChannel(&hadc1,&chadc1);
  
  ADC->CCR|=ADC_CCR_TSVREFE; //Temperature sensor enable
  
  HAL_ADC_Start(&hadc1);
  HAL_TIM_Base_Start(&htim2);
  HAL_ADC_Start_DMA(&hadc1, Data, 60);
  
  DMA2_Stream0->CR &= ~DMA_SxCR_HTIE;   // Disable half transfer interrupt
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  
  while(1);
}

void DMA2_Stream0_IRQHandler(void)
{
  DMA2->LIFCR |= DMA_LIFCR_CTCIF0; //clear flag
  HAL_GPIO_TogglePin(GPIOG,GPIO_PIN_13);
  T = HAL_ADC_GetValue(&hadc1);
}